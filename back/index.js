const config = require('./config.json');
const fs = require('fs');
const path = require('path');
const express = require('express')
const app = express()
const port = 3000

const play = (req, res) => {
    res.send('Playing sound!');
};

const list = (req, res) => {
    const promise = new Promise((resolve, reject) => {
        return fs.readdir(
            config.soundsPath,
            (err, filenames) =>
                err != undefined ?
                    reject(err)
                    : resolve(filenames)
        )
    });

    promise
        .then(files => {
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(
                files
                    .filter(file => path.extname(file) === '.ogg')
                    .map(file => path.parse(file).name)
            ));
        })
        .catch(err => {
            res.status(500);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(err));
        });
};

app.get('/api/sounds', list);
app.post('/api/sounds/:filename/play', play);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
